<?php
    function h($s){
        return htmlspecialchars($s, ENT_QUOTES,'UTF-8');
    }
    try {
        if(!isset($_GET['t']) || !preg_match('/\A\d{4}-\d{2}\z/', $_GET['t'])){
            throw new Exception();
        }
        $thisMonth = new DateTime($_GET['t']);

    } catch(Exception $e){
        $thisMonth = new DateTime('first day of this month');
    }
    $dt = clone $thisMonth;
    $prev = $dt->modify('-1 month')->format('Y-m');
    $dt = clone $thisMonth;
    $next = $dt->modify('+1 month')->format('Y-m');

    $yearMonth = $thisMonth -> format('F Y');
    $tail =' ';
    $lastDayofPrevMonth = new DateTime('last day of'. $yearMonth .'-1 month');
    while($lastDayofPrevMonth->format('w')<6){
             $tail=sprintf('<td style= color:#dedede;>%d</td>',$lastDayofPrevMonth->format('d')).$tail;
            $lastDayofPrevMonth -> sub(new DateInterval('P1D'));
    }
    $body =' ';
    $period = new DatePeriod(
        new DateTime('first day of'. $yearMonth),
        new DateInterval('P1D'),
        new Datetime('first day of'. $yearMonth. '+1 month')
    );
    $today = new DateTime('today');
    foreach($period as $day){
        if($day->format('w')%7===0){
            $body .='</tr><tr>';
        };
        if($day->format('Y-m-d')===$today->format('Y-m-d')){
              $body .=sprintf('<td style= color:red;>%d </td>',$day->format('d'));
         }else{
             $body .=sprintf('<td>%d </td>',$day->format('d'));
         }
    }
    $head = ' ';
    $firstDayofNextMonth = new DateTime('first day of' . $yearMonth .'+1 month');
    while($firstDayofNextMonth->format('w')>0){
        $head .= sprintf('<td style= color:#dedede>%d</td>',$firstDayofNextMonth->format('d'));
        $firstDayofNextMonth->add(new DateInterval('P1D'));
    }
    $html = '<tr>' . $tail . $body . $head . '</tr>';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>quiz</title>
    <link rel="stylesheet" href="styles.css">
</head>
<body>
    <table>
        <thead>
            <tr>
                <th><a href="http://localhost/exercise/calender/calender.php/?t=<?php echo h($prev)?>">previous</a></th>
                <th><?php echo h($yearMonth) ?></th>
                <th><a href="http://localhost/exercise/calender/calender.php/?t=<?php echo h($next)?>">next</a></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Sunday</td>
                <td>Monday</td>
                <td>Tuesday</td>
                <td>Wednesday</td>
                <td>Thursday</td>
                <td>Friday</td>
                <td>Saturday</td>
            </tr>
            <?php echo $html?>
        </tbody>
        <tfoot>
            <th><a href="">today</th>
        </tfoot>
    </table>
</body>
</html>
