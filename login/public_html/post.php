<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');


$app = new MyApp\Controller\Post();

$app->run();

//$app->me()
//$app->getValues()->users
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Post</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
  <div id="container">
    <form action="logout.php" method="post" id="logout">
      <P><?= h($app->me() ->username); ?> is active now</P> <input type="submit" value="Log Out">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <form action="" method="post" enctype="multipart/form-data" id ="post">
      <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo h(MAX_FILE_SIZE); ?>">
      <textarea class="textlines" placeholder="What's happening?" name="content"></textarea><br>
      <input type="file" name="image">
      <input type="submit" value="upload">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
      <p class ="err"><?= h($app -> getErrors('content')); ?></p>
      <p class ="err"><?= h($app -> getErrors('picture')); ?></p>


    </form>



    </ul>
  </div>
</body>
</html>
