<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');


$app = new MyApp\Controller\Search();

$app->run();
$image_pathdir = "/login/images/"

//$app->me()
//$app->getValues()->users
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Search</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
  <div id="container">
    <form action="logout.php" method="post" id="logout">
        <P><?= h($app->me() ->username); ?> is active now</P> <input type="submit" value="Log Out">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <div class="search">
    <form action="" method="post">
        <label for="username">username：</label>
        <input type="text" name="username" id="name"><input type="submit" name="submit" value="search">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <form action="" method="post">
        <label for="post">post：</label>
        <input type="text" name="post" id="post"><input type="submit" name="submit" value="search">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    </div>
    <p class ="err"><?= h($app -> getErrors('username')); ?></p>
    <p class ="err"><?= h($app -> getErrors('post')); ?></p>
  <?php if (isset($app->getValues()->usersname)) : ?>
    <h1>Users <span class="fs12">(<?= count($app->getValues()->usersname)?>)</span></h1>
    <ul>
          <?php foreach ($app->getValues()->usersname as $user) :?>
            <li><?= h($user->email); ?></li>
          <?php endforeach; ?>
    </ul>
  <?php endif ; ?>
  <?php if (isset($app->getValues()->post)) : ?>
    <h1>Posts <span class="fs12">(<?= count($app->getValues()->post)?>)</span></h1>
    <ul>
          <?php foreach ($app->getValues()->post as $post) :?>
            <li><?= h($post->content); ?>
                <?php if (isset($post->image_path)) : ?>
                <a href="<?php echo h($image_pathdir). h(basename($post->image_path)); ?>">
                    <img src="<?php echo h($image_pathdir). h(basename($post->image_path)); ?>">
                </a>
                <?php endif; ?>
            </li>
          <?php endforeach; ?>
    </ul>
  <?php endif ; ?>
  </div>
</body>
</html>
