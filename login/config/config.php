<?php

ini_set('display_errors', 1);
//database
define('DSN', 'mysql:dbname=microblog;host=localhost:3306;charset=utf8mb4');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', 'root');
//imagefile
define('MAX_FILE_SIZE', 1 * 1024 * 1024); // 1MB
define('THUMBNAIL_WIDTH', 400);
define('IMAGES_DIR', __DIR__ . '/../images');
define('THUMBNAIL_DIR', __DIR__ . '/../thumbs');

define('SITE_URL', 'localhost:8080/login/');

require_once(__DIR__ . '/../lib/functions.php');
require_once(__DIR__ . '/autoload.php');

session_start();
