<?php

namespace MyApp\Model;
class Follow extends \MyApp\Model {
    public function getfollow_db() {
        $stmt = $this->db->prepare("select * from follow where user_id = :user_id");
        $stmt->execute([
            ':user_id' => $_SESSION['me']->user_id
        ]);
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
        return $stmt->fetchAll();
    }
    public function check_db1($values) {
        $stmt = $this->db->prepare("select * from follow where user_id = :user_id and follow_id = :follow_id");
        $stmt->execute([
            ':user_id' => $_SESSION['me']->user_id,
            ':follow_id' => $values['follow_id']
        ]);
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
        return $stmt->fetchAll();
    }
    public function check_db2($values) {
        $stmt = $this->db->prepare("select * from follow where user_id = :user_id and follow_id = :follow_id");
        $stmt->execute([
            ':user_id' => $_SESSION['me']->user_id,
            ':follow_id' => $values['unfollow_id']
        ]);
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
        return $stmt->fetchAll();
    }
    public function insertFollow($values){
    $stmt = $this->db->prepare("insert into follow (user_id,follow_id) values (:user_id,:follow_id)");
      $res = $stmt -> execute([
          ':user_id' => $_SESSION['me']->user_id,
          ':follow_id' => $values['follow_id']
      ]);
      if($res === false){
          throw new \MyApp\Exception\DatabaseError();
      }
    }
    public function delete($values){
    $stmt = $this->db->prepare("delete from follow where user_id = :user_id and follow_id = :follow_id");
      $res = $stmt -> execute([
          ':user_id' => $_SESSION['me']->user_id,
          ':follow_id' => $values['follow_id']
      ]);
      if($res === false){
          throw new \MyApp\Exception\DatabaseError();
      }
    }
}
 ?>
