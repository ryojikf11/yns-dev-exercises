<?php

namespace MyApp\Model;

class Posts extends \MyApp\Model {
    public function postContent($values){
    $stmt = $this->db->prepare("insert into post (content,created,modified,user_id) values (:content ,now(),now(),:user_id)");
      $res = $stmt -> execute([
          ':content' => $values['content'],
          ':user_id' => $values['user_id']
      ]);
      if($res === false){
          throw new \MyApp\Exception\DatabaseError();
      }
    }
    public function postPicture($values){
    $stmt = $this->db->prepare("insert into post (image_path,created,modified,user_id) values (:picturePath ,now(),now(),:user_id)");
      $res = $stmt -> execute([
          ':picturePath' => $values['picturePath'],
          ':user_id' => $values['user_id']
      ]);
      if($res === false){
          throw new \MyApp\Exception\DatabaseError();
      }
    }
    public function postBoth($values){
    $stmt = $this->db->prepare("insert into post (content,image_path,created,modified,user_id) values (:content,:picturePath ,now(),now(),:user_id)");
      $res = $stmt -> execute([
          ':content' => $values['content'],
          ':picturePath' => $values['picturePath'],
          ':user_id' => $values['user_id']
      ]);
      if($res === false){
          throw new \MyApp\Exception\DatabaseError();
      }
    }
    public function searchPost_DB($values) {
        $stmt = $this->db->prepare("select * from post where content like :post");
        $bindvalue = "%" .$values['post'] . "%";
        $stmt->execute([
            ':post' => $bindvalue
        ]);
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
        return $stmt->fetchAll();
    }
    public function getAllPost(){
        $stmt = $this->db->prepare("select * from post where content like :post");
        $bindvalue = "%" .$values['post'] . "%";
        $stmt->execute([
            ':post' => $bindvalue
        ]);
        $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
        return $stmt->fetchAll();
    }
}

 ?>
