<?php

namespace MyApp\Controller;

class Search extends \MyApp\Controller {

    public function run() {
        if (!$this->isLoggedIn()) {
          // login
          header('Location' . SITE_URL . '/login.php');
          exit;
        }
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            if(isset($_POST["username"])){
                $this->searchUsername();
                // $this->setUsername();
            }
            if(isset($_POST["post"])){
                $this->searchPost();
            }
        }

    }

    protected function searchPost() {
        try {
            $this->_validatePost();
        }catch (\MyApp\Exception\InvalidInput $e){
            $this->setErrors('post',$e->getMessage());
        }
        if($this->hasError()) {
            return;
        }else {
            try {
              $postsModel = new \MyApp\Model\Posts();
              $searchedPost = $postsModel->searchPost_DB([
                'post' => $_POST['post']
              ]);
              $this ->setValues('post', $searchedPost);
          } catch (\MyApp\Exception\NoResult $e) {
              $this->setErrors('post', $e->getMessage());
              return;
          }
        }

    }
    protected function searchUsername() {
        try {
            $this->_validateUsername();
        }catch (\MyApp\Exception\InvalidInput $e){
            $this->setErrors('username',$e->getMessage());
        }
        if($this->hasError()) {
            return;
        }else {
            try {
              $userModel = new \MyApp\Model\User();
              $usersname = $userModel->searchUsername_DB([
                'username' => $_POST['username']
              ]);
              $this ->setValues('usersname', $usersname);
          } catch (\MyApp\Exception\NoResult $e) {
              $this->setErrors('username', $e->getMessage());
              return;
          }
        }


    }
    private function _validateUsername() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if($_POST['username'] === null){
            throw new \MyApp\Exception\InvalidInput();
         }
         if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['username'])){
            throw new \MyApp\Exception\InvalidInput();
         }
    }
    private function _validatePost() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if($_POST['post'] === null){
            throw new \MyApp\Exception\InvalidInput();
         }
         if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['post'])){
            throw new \MyApp\Exception\InvalidInput();
         }
    }
}
