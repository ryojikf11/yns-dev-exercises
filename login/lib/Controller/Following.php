<?php

namespace MyApp\Controller;

class Following extends \MyApp\Controller {

    public function run() {
        if (!$this->isLoggedIn()) {
          // login
          header('Location' . SITE_URL . '/login.php');
          exit;
        }
        $this->getAllInfo();

        $this->getFollow();
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->dis_followUnfollow();
            header('Location:http://localhost:8080/login/public_html/follow.php');
            return;
        }

    }
    protected function getAllInfo() {
          $userModel = new \MyApp\Model\User();
          $this->setValues('users',$userModel->findAll());
    }
    protected function getFollow() {
        try {
          $followModel = new \MyApp\Model\Follow();
          $follow_users = $followModel->getfollow_db([
            'user_id' => $this->me()->user_id,
          ]);
      } catch (\MyApp\Exception\DatabaseError $e) {
          $this->setErrors('follow', $e->getMessage());
          return;
      }
         $this->setValues('follow',$follow_users);
    }

    protected function dis_followUnfollow() {
        $this->_validate();
        if(isset($_POST['follow'])){
            // $this->check_count1();
            $this->follow_Process();
        }
        if(isset($_POST['unfollow'])){
            // $this->check_count2();
            $this->unfollow_process();
        }
    }
    private function _validate() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
        }
    }
    protected function follow_process() {
        try {
            $insertModel = new \MyApp\Model\Follow();
            $insertModel->insertFollow([
                'user_id' => $_SESSION['me'] ->user_id,
                'follow_id' =>$_POST['follow']
            ]);
        } catch (\MyApp\Exception\DatabaseError $e) {
            $this->setErrors('follow', $e->getMessage());
            return;
        }
    }
    protected function unfollow_process() {
        try {
            $deleteModel = new \MyApp\Model\Follow();
            $deleteModel->delete([
                'user_id' => $_SESSION['me'] ->user_id,
                'follow_id' =>$_POST['unfollow']
            ]);
        } catch (\MyApp\Exception\DatabaseError $e) {
            $this->setErrors('follow', $e->getMessage());
            return;
        }
    }
    protected function check_count1() {
        try {
          $countModel = new \MyApp\Model\Follow();
          $count = $countModel->check_db1([
              'user_id' => $_SESSION['me']->user_id,
              'follow_id' => $_POST['follow']
          ]);
      } catch (\MyApp\Exception\DatabaseError $e) {
          $this->setErrors('follow', $e->getMessage());
          return;
      }
      var_dump($count);
    }
    protected function check_count2() {
        try {
          $countModel = new \MyApp\Model\Follow();
          $count = $countModel->check_db2([
              'user_id' => $_SESSION['me']->user_id,
              'unfollow_id' => $_POST['unfollow']
          ]);
      } catch (\MyApp\Exception\DatabaseError $e) {
          $this->setErrors('follow', $e->getMessage());
          return;
      }
      var_dump($count);
    }

}
?>
