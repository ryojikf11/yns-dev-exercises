<?php

namespace MyApp\Controller;

class Mainpage extends \MyApp\Controller {

  public function run() {
    if (!$this->isLoggedIn()) {
      // login
      header('Location' . SITE_URL . '/login.php');
      exit;
    }

    // get users info
    $userModel = new \MyApp\Model\Posts();
    $this->setValues('users',$userModel->findAll());
  }

}
