<?php

namespace MyApp\Controller;

class Post extends \MyApp\Controller {

  public function run() {
      // get users info
    $userModel = new \MyApp\Model\User();
    $content;
    $savePath;
    if (!$this->isLoggedIn()) {
      // login
      header('Location' . SITE_URL . '/login.php');
      exit;
    }
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        if ($_POST['content'] && $_FILES['image']['size'] !== 0) {
            $this->set_post();
            $this->set_pic();
            $this->createBoth();
        };
        if($_POST['content'] && $_FILES['image']['size'] === 0){
             $this->set_post();
             $this->createContent();
        };
        if(!$_POST['content'] && $_FILES['image']['size'] !== 0){
             $this->set_pic();
             $this->createPicture();
        }
    }
  }
  protected function set_post() {
      //validate
      try {
          $this -> _validate();
      } catch (\MyApp\Exception\InvalidContent $e){
          $this->setErrors('content',$e->getMessage());
      }
      $this->content = $_POST['content'];
  }
  protected function set_pic() {
      try {

        // error check
        $this->_validateUpload();

        // type check
        $ext = $this->_validateImageType();
        // save
        $savePath =$this->_save($ext);
        // create thumbnail
        // $this->_createThumbnail($savePath);
        // $_SESSION['success'] = 'Upload Done!';
      } catch (\MyApp\Exception\InvalidPicture $e) {

          $this->setErrors('content',$e->getMessage());
        // exit;
      }
      $this->savePath =$savePath;
      // redirect
       // header('Location: http://localhost:8080/login/public_html/post.php');
       // exit;
  }
    private function _validateUpload() {
        if(!isset($_FILES['image']) || !isset($_FILES['image']['error'])){
            throw new \MyApp\Exception\InvalidPicture();
        }
        switch($_FILES['image']['error']){
            case UPLOAD_ERR_OK:
            return true;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
            throw new \MyApp\Exception\InvalidPicture();
            default:
            throw new \MyApp\Exception\InvalidPicture();
        }
      }

    private function _validateImageType(){
        $this->_imageType = exif_imagetype($_FILES['image']['tmp_name']);
        switch($this->_imageType){
            case IMAGETYPE_GIF:
            return 'gif';
            break;
            case IMAGETYPE_JPEG:
            return 'jpeg';
            break;
            case IMAGETYPE_PNG:
            return 'png';
            break;
            default:
            throw new \MyApp\Exception\InvalidPicture();
        }
    }

    private function _save($ext) {
        $this->_imageFileName = sprintf(
            '%s_%s.%s',
            time(),
            sha1(uniqid(mt_rand(), true)),
            $ext
        );
        $uploaddir = '/var/www/html/login/images/';
        $savePath = $uploaddir  . $this->_imageFileName;
        $res = move_uploaded_file($_FILES['image']['tmp_name'], $savePath);
        if($res === false) {
            throw new \MyApp\Exception\InvalidPicture();
            // throw new \Exception('Could not upload!');
        }
        return $savePath;
    }
    // private function _createThumbnail($savePath){
    //     $imageSize = getimagesize($savePath);
    //     $width = $imageSize[0];
    //     $height = $imageSize[1];
    //     if($width > THUMBNAIL_WIDTH){
    //         $this-> _createThumbnailMain($savePath,$width,$height);
    //     }
    // }
    // private function _createThumbnailMain($savePath,$width,$height) {
    //     switch($this->_imageType){
    //         case IMAGETYPE_GIF:
    //         $srcImage = imagecreatefromgif($savePath);
    //         break;
    //         case IMAGETYPE_JPEG:
    //         $srcImage = imagecreatefromjpeg($savePath);
    //         break;
    //         case IMAGETYPE_PNG:
    //         $srcImage = imagecreatefrompng($savePath);
    //         break;
    //     }
    //     $thumbHeight = round($height * THUMBNAIL_WIDTH / $width);
    //     $thumbImage = imagecreatetruecolor(THUMBNAIL_WIDTH, $thumbHeight);
    //     imagecopyresampled($thumbImage, $srcImage, 0, 0, 0, 0, THUMBNAIL_WIDTH, $thumbHeight, $width, $height);
    //
    //   switch($this->_imageType) {
    //     case IMAGETYPE_GIF:
    //       imagegif($thumbImage, THUMBNAIL_DIR . '/' . $this->_imageFileName);
    //       break;
    //     case IMAGETYPE_JPEG:
    //       imagejpeg($thumbImage, THUMBNAIL_DIR . '/' . $this->_imageFileName);
    //       break;
    //     case IMAGETYPE_PNG:
    //       imagepng($thumbImage, THUMBNAIL_DIR . '/' . $this->_imageFileName);
    //       break;
    //   }
    // }

    private function _validate() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['content'])){
            throw new \MyApp\Exception\InvalidContent();
         }
    }
    protected function createBoth() {
        $setContent = $this->content;
        $setPicture = $this->savePath;
        // $this ->setValues('picturePath', $setPicture);
        if($this->hasError()) {
            return;
        } else {
            //create post
            try {
                $userModel = new \MyApp\Model\Posts();
                $userModel->postBoth([
                    'content' => $setContent,
                    'picturePath' => $setPicture,
                    'user_id' => $_SESSION['me']->user_id
                ]);
            } catch (\MyApp\Exception\DatabaseError $e) {
                $this->setErrors('content', $e->getMessage());
                return;
            }

            //redirect to login//need to revise
            // header('Location:http://localhost:8080/login/public_html/login.php');
            // return;
        }
    }
    protected function createPicture(){
        $setPicture = $this->savePath;
        $this ->setValues('picturePath', $setPicture);
        if($this->hasError()) {
            return;
        } else {
            //create post
            try {
              $userModel = new \MyApp\Model\Posts();
              $userModel->postPicture([
                'picturePath' => $setPicture,
                'user_id' => $_SESSION['me']->user_id
              ]);
          } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('content', $e->getMessage());
              return;
            }

        //redirect to login//need to revise
        // header('Location:http://localhost:8080/login/public_html/login.php');
        // return;
        }
    }
    protected function createContent(){
        $setContent = $this->content;
        if($this->hasError()) {
            return;
        } else {
            //create post
            try {
              $userModel = new \MyApp\Model\Posts();
              $userModel->postContent([
                'content' => $setContent,
                'user_id' => $_SESSION['me']->user_id
              ]);
          } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('content', $e->getMessage());
              return;
            }

        //redirect to login//need to revise
        // header('Location:http://localhost:8080/login/public_html/login.php');
        // return;
        }
    }

}
