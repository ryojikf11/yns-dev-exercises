<?php

// 新規登録

require_once(__DIR__ . '/../config/config.php');

$app = new MyApp\Controller\Profile();
$profile_pathdir = "/../profile/";
$app->run();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Profile</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
    <header>
        <h1 class="main_title">Microblog</h1>
    </header>
    <form action="logout.php" method="post" id="logout">
      <P><?= h($app->me() ->username); ?> is active now</P> <input type="submit" value="Log Out">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>

    <div id="container">
        <form action="" enctype="multipart/form-data" method="post">
            <p>
                username <input type='text' name="username" value="<?= h($_SESSION['me']->username)?>">
            </p>
                <p class ="err"><?= h($app -> getErrors('username')); ?></p>
            <p>
                email <input type='text' name="email" value="<?= h($_SESSION['me'] ->email)?>">
            </p>
            <?php if(!empty($_SESSION['me']->profile_image)) :?>
                <p>current profile image</p>
                <img src="<?php echo h($profile_pathdir). h(basename($_SESSION['me']->profile_image)); ?>" height="50" width="50"><br>
            <?php endif ;?>
            <input type="file" name="image">
            <p class ="err"><?= h($app -> getErrors('email')); ?></p>
            <p class ="err"><?= h($app -> getErrors('password')); ?></p>
            <button type = "submit">change the data</button><br>
            <button type="submit" name="return">return</button><br>
            <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
        </form>
  </div>
</body>
</html>
