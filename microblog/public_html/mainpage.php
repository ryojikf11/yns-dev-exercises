<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');


$app = new MyApp\Controller\Mainpage();

$app->run();
if(!isset($app->getvalues()->posts)){
    echo 'please follow other users';
}
$image_pathdir = "/../images/";
$profile_pathdir = "/../profile/";
$favArray = array();
foreach($app->getvalues()->mylikes as $mylike){
    array_push($favArray,$mylike->post_id);
}
// $canforeach = count($app->getvalues()->myretweets);
// $retArray = array();
// foreach($app->getvalues()->myretweets as $myretweet){
    // echo $myretweet->user_id;
    // array_push($retArray,$myretweet->post_id);
// }
// $countArray = array();
// foreach($app->getvalues()->countLikes as $countLike){
//     $countArray += array($countLike->post_id => $countLike->check);
// }
// foreach($countArray as $key => $value){
//     echo $key.'"' ;
// }
//pagenation
define('MAX','10');
$data_num = count($app->getvalues()->posts);
$max_page = ceil($data_num / MAX);
if(!isset($_GET['page_id'])){
  $now =1;
}else{
  $now = $_GET['page_id'];
}

$start_no = ($now - 1)* MAX;
$limit = $start_no + 10;
$count = 0;
$true = 0;

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Mainpage</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
    <?php if(isset($_SESSION['successLog'])) :?>
    <p style="color:blue"><?php echo h($_SESSION['successLog']); ?></p>
    <?php unset($_SESSION['successLog']); ?>
    <?php endif ;?>
    <?php if(isset($_SESSION['successPos'])) :?>
    <p style="color:blue"><?php echo h($_SESSION['successPos']); ?></p>
    <?php unset($_SESSION['successPos']); ?>
    <?php endif ;?>
    <?php if(isset($_SESSION['successCom'])) :?>
    <p style="color:blue"><?php echo h($_SESSION['successCom']); ?></p>
    <?php unset($_SESSION['successCom']); ?>
    <?php endif ;?>
    <header>
        <h1 class="main_title">Microblog</h1>
        <div id ="nav">
            <ul>
                <li><a href="post.php">post</a></li>
                <li>mainpage</li>
                <li><a href="search.php">Search</a></li>
                <li><a href="mypage.php">mypage</a></li>
            </ul>
    </div>
    </header>
  <div id="container">
    <form action="logout.php" method="post" id="logout">
      <?= h($app->me() ->email); ?> <input type="submit" value="Log Out">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
  </div>
  <div>
    <form action="" method="post" name="myform">
    <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    <?php if (isset($app->getvalues()->posts)) : ?>
        <h1>Posts<span>(<?= count($app->getValues()->posts)?>)</span></h1>
          <ul>
                <?php foreach($app->getvalues()->posts as $post) : ?>
                    <?php if($count >= $limit) :?>
                        <?php break ;?>
                    <?php endif ; ?>
                        <?php if($count >= $start_no) :?>
                        <li class="post">
                            <?php if($post->status == 0) :?>
                                <p>this post has arleady been deleted</p>
                            <?php else :?>
                            <div style="text-align: left;position: absolute">
                                <p>posted by <?php echo h($post->username) ?></P>
                                <?php if(isset($post->profile_image)) :?>
                                    <img src="<?php echo h($profile_pathdir). h(basename($post->profile_image)); ?>" height="50" width="50"><br>
                                <?php endif ; ?>
                                </div>
                                <?php if(isset($post->content)) :?>
                                    <p><?php echo h($post->content); ?></P>
                                <?php endif; ?>
                                <?php if(isset($post->image_path)) :?>
                                    <img src="<?php echo h($image_pathdir). h(basename($post->image_path)); ?>" height="250"><br>
                                <?php endif ; ?>

                                <?php foreach($app->getvalues()->countLikes as $countryLike): ?>
                                    <?php if($countryLike->post_id === $post->important) :?>
                                        <p>favorite count(<?php echo h($countryLike->check) ;?>) </p>
                                        <?php $true = 1; ?>
                                    <?php endif ;?>
                                    <?php endforeach ;?>
                                <?php if($true === 0) :?>
                                    <p><?php echo 'nobody favorite' ;?></p>
                                <?php endif ;?>
                                <?php if(empty($app->getvalues()->mylikes)) : ?>
                                    <button type="submit" name="favorite" value="<?php echo $post->important ?>">favorite</button>
                                <?php else :?>
                                    <?php if(in_array($post->important,$favArray,true)) : ?>
                                        <button type="submit" name="unfavorite" value="<?php echo $post->important ?>">unfavorite</button>
                                    <?php else :?>
                                        <button type="submit" name="favorite" value="<?php echo $post->important ?>">favorite</button>
                                    <?php endif ;?>
                                <?php endif ;?>
                                <button type="submit" name="comment" value="<?php echo $post->important ?>">comment</button>
                                <?php if($post->user_id === $_SESSION['me']->user_id && !isset($post->retweet)): ?>
                                    <button  type = "submit" name="delete" value="<?php echo $post->important ?>">delete</button>
                                    <button type="submit" name="edit" value="<?php echo $post->important ?>">edit</button>
                                <?php endif; ?>
                                <?php if(empty($app->getvalues()->myretweets)) : ?>
                                    <button type="submit" name="retweet" value="<?php echo $post->important ?>">retweet</button>
                                <?php else :?>
                                        <?php foreach($app->getvalues()->myretweets as $myretweet): ?>
                                            <?php $boolRetweet = 0 ;?>
                                            <?php if($myretweet->post_id === $post->important && isset($post->retweet) && $_SESSION['me']->username === $post->retweetby): ?>
                                                <button type="submit" name="unretweet" value="<?php echo $post->important ?>">unretweet</button>
                                                <p>retweeted  by yourself</p>
                                                <?php $boolRetweet = 1 ?>
                                                <?php break ;?>
                                            <?php elseif($myretweet->post_id === $post->important && isset($post->retweet) && $_SESSION['me']->username !== $post->retweetby)  :?>
                                                <p>retweeted by <?php echo h($post->retweetby) ;?></p>
                                                <?php $boolRetweet = 1 ?>
                                                <?php if($myretweet->post_id === $post->important && isset($post->retweet)) :?>
                                                    <p>you cant retweet same post again</p>
                                                <?php endif ;?>
                                                <?php break ;?>
                                            <?php elseif($myretweet->post_id === $post->important && !isset($post->retweet))  :?>
                                                <p>you cant retweet same post again</p>
                                                <?php $boolRetweet = 1 ;?>
                                                <?php break ;?>
                                            <?php endif ;?>
                                        <?php endforeach ; ?>
                                        <?php if($boolRetweet === 0) :?>
                                            <button type="submit" name="retweet" value="<?php echo $post->important ?>">retweet</button>
                                        <?php endif; ?>
                                <?php endif ;?>
                                <?php foreach($app->getvalues()->comments as $comment) : ?>
                                    <ul >
                                    <?php if($post->important === $comment->post_id) : ?>
                                        <li class="post"><p>comment by <?php echo h($comment->username) ;?></P>
                                            <?php echo h($comment->comment) ;?>
                                    <?php endif ; ?>
                                    </ul>
                            <?php endforeach ;?>
                        <?php endif ;?>
                        </li>
                    <?php endif ;?>
                        <?php $count ++ ?>
                <?php endforeach ;?>
            </ul>
    <?php endif ;?>
    </form>
    <?php for($o = 1; $o <= $max_page; $o++) :?>
        <?php if($o == $now) :?>
            <?php echo $now. ' ' ;?>
        <?php else :?>
            <?php echo '<a href=\'/public_html/mainpage.php?page_id='. $o. '\')>'. $o. '</a>'. '　'; ;?>
        <?php endif ;?>
    <?php endfor ;?>

  </div>
  <script type="text/javascript">
        // function myFunction(){
        //     var result = confirm('are you sure')
        //     if(result){
        //         window.location.href = '/exercise/microblog/public_html/mainpage.php';
        //     }
        // }
  </script>



      <p class ="err"><?= h($app -> getErrors('favorite')); ?></p>
</body>
</html>
