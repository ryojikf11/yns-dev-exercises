<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');


$app = new MyApp\Controller\Following();

$app->run();

    $array_follow = array();
    foreach ($app->getValues()->follow as $follow) {
        array_push($array_follow,$follow->follow_id);
    }

?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Follow</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
    <header>
    <h1>Microblog</h1>
    <nav>
    <ul>
    <li><a href="post.php">Post</a></li>
    <li><a href="mainpage.php">mainpage</a></li>
    <li><a href="search.php">Search</a></li>
    <li>follow&unfollow</li>
    <li><a href="mypage.php">mypage</a></li>
    </ul>
    </nav>
    </header>
  <div id="container">
    <form action="logout.php" method="post" id="logout">
      <?= h($app->me() ->email); ?> <input type="submit" value="Log Out">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <form action="" method="post">
    <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    <h1>Follow & Unfollow<span class="fs12">(<?= count($app->getValues()->users)?>)</span></h1>
        <ul>
          <?php foreach ($app->getValues()->users as $user) :?>
            <li><?= h($user->email); ?>
                <?php if (in_array($user->user_id, $array_follow, true)) :?>
                <button type="submit" name="unfollow" value="<?php echo $user->user_id ?>">Unfollow</button>
                <?php else: ?>
                <button type="submit" name="follow" value="<?php echo $user->user_id ?>">Follow</button>
                <?php endif; ?>
            </li>
        <?php endforeach; ?>
        </ul>
    </form>
  </div>
    <p class="err"><?= h($app->getErrors('follow')); ?></p>
</body>
</html>
