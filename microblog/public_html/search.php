<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');



$app = new MyApp\Controller\Search();

$app->run();
if(isset($app->getValues()->follow)){
    $array = [];
    foreach ($app->getValues()->follow as $follow){
        array_push($array,$follow->user_id);
    }
}

$image_pathdir = "/../images/"
//$app->me()
//$app->getValues()->users
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Search</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
    <header>
        <h1 class="main_title">Microblog</h1>
        <div id ="nav">
        <ul>
        <li><a href="post.php">Post</a></li>
        <li><a href="mainpage.php">mainpage</a></li>
        <li>Search</li>
        <li><a href="mypage.php">mypage</a></li>
        </ul>
    </header>
    <form action="logout.php" method="post" id="logout">
        <P><?= h($app->me() ->username); ?> is active now</P> <input type="submit" value="Log Out">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <div class="search">
    <form action="" method="post">
        <label for="username">username:</label>
        <input type="text" name="username" id="name"><input type="submit" name="submit" value="search">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <form action="" method="post">
        <label for="post">post:</label>
        <input type="text" name="post" id="post"><input type="submit" name="submit" value="search">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    </div>
    <p class ="err"><?= h($app -> getErrors('username')); ?></p>
    <p class ="err"><?= h($app -> getErrors('post')); ?></p>
  <?php if (isset($app->getValues()->usersname)) : ?>
    <form action="" method="post">
    <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    <h1>Users <span class="fs12">(<?= count($app->getValues()->usersname)?>)</span></h1>
    <ul>
          <?php foreach ($app->getValues()->usersname as $user) :?>
            <li><?= h($user->username); ?>
                <?php if($user->user_id === $_SESSION['me']->user_id) :?><p>me</P>
                <?php elseif(in_array($user->user_id,$array,true)): ?><button type="submit" name="unfollow" value="<?php echo $user->user_id ?>">Unfollow</button>
                <?php else :?><button type="submit" name="follow" value="<?php echo $user->user_id ?>">Follow</button>
                <?php endif ;?>
            </li>
          <?php endforeach; ?>
    </ul>
    </form>
  <?php endif ; ?>
  <?php if (isset($app->getValues()->post)) : ?>
    <h1>Posts <span class="fs12">(<?= count($app->getValues()->post)?>)</span></h1>
    <ul>
          <?php foreach ($app->getValues()->post as $post) :?>
            <li class="post"><?= h($post->content); ?>
                posted by <?= h($post->username); ?>
                <?php if (isset($post->image_path)) : ?>
                    <img src="<?php echo h($image_pathdir). h(basename($post->image_path)); ?>" height="250">
                </a>
                <?php endif; ?>
            </li>
          <?php endforeach; ?>
    </ul>
  <?php endif ; ?>
</body>
</html>
