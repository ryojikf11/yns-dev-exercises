<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');


$app = new MyApp\Controller\Mypage();

$app->run();
$image_pathdir = "/../images/";
//$app->me()
//$app->getValues()->users
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>index</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
    <?php if(isset($_SESSION['successPro'])) :?>
    <p style="color:blue"><?php echo h($_SESSION['successPro']); ?></p>
    <?php unset($_SESSION['successPro']); ?>
    <?php endif ;?>
    <header>
        <h1 class="main_title">Microblog</h1>
        <div id ="nav">
        <ul>
        <li><a href="post.php">post</a></li>
        <li><a href="mainpage.php">mainpage</a></li>
        <li><a href="search.php">Search</a></li>
        <li>mypage</li>
        </ul>
    </header>
  <div id="container">
    <form action="logout.php" method="post" id="logout">
      <?= h($app->me() ->email); ?> <input type="submit" value="Log Out">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <h3><a href="profile.php">Edit Profile</a></h3>
    <h1>MY Follow <span class="fs12">(<?= count($app->getValues()->follow)?>)</span></h1>
    <?php if(isset($app->getValues()->follow)) :?>
        <ul>
          <?php foreach ($app->getValues()->follow as $follow) :?>
            <li><?= h($follow->username); ?></li>
        <?php endforeach; ?>
        </ul>
    <?php else :?> <P>follow your friends</P>
    <?php endif ;?>

    <?php if(isset($app->getValues()->post)) :?>
        <h1>MY Post <span class="fs12">(<?= count($app->getValues()->post)?>)</span></h1>
        <ul>
          <?php foreach ($app->getValues()->post as $post) :?>
              <li><?php if (isset($post->content)) : ?>
                    <p><?php echo h($post->content); ?></P>
                  <?php endif; ?>
                  <?php if (isset($post->image_path)) : ?>
                      <img src="<?php echo h($image_pathdir). h(basename($post->image_path)); ?>"  height="250">
                  </a>
                  <?php endif; ?>
              </li>
              </li>
        <?php endforeach; ?>
        </ul>
    <?php else :?> <P>Please Post</P>
    <?php endif; ?>
    <?php if(isset($app->getValues()->favorite)) :?>
        <h1>MY Favorite <span class="fs12">(<?= count($app->getValues()->favorite)?>)</span></h1>
        <ul>
          <?php foreach ($app->getValues()->favorite as $favorite) :?>
              <li><?php if (isset($favorite->content)) : ?>
                    <p><?php echo h($favorite->content); ?></P>
                  <?php endif; ?>
                  <?php if (isset($favorite->image_path)) : ?>
                      <img src="<?php echo h($image_pathdir). h(basename($favorite->image_path)); ?>"  height="250">
                  </a>
                  <?php endif; ?>
              </li>
              </li>
        <?php endforeach; ?>
        </ul>
    <?php else :?> <P>Please Post</P>
    <?php endif; ?>

    </ul>

  </div>
</body>
</html>
