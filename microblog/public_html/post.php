<?php

// ユーザーの一覧

require_once(__DIR__ . '/../config/config.php');


$app = new MyApp\Controller\Post();

$app->run();
//$app->me()
//$app->getValues()->users
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="utf-8">
  <title>Post</title>
  <link rel="stylesheet" href="styles2.css">
</head>
<body>
    <header>
    <h1 class="main_title">Microblog</h1>
    <div id ="nav">
        <ul>
            <li>Post</li>
            <li><a href="mainpage.php">mainpage</a></li>
            <li><a href="search.php">Search</a></li>
            <li><a href="mypage.php">mypage</a></li>
        </ul>
    </div>
    </header>


    <form action="logout.php" method="post" id="logout">
      <P><?= h($app->me() ->username); ?> is active now</P> <input type="submit" value="Log Out">
      <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <form action="" method="post" enctype="multipart/form-data" id ="post">
        <input type="hidden" name="MAX_FILE_SIZE" value="<?php echo h(MAX_FILE_SIZE); ?>">
        <textarea class="textlines" placeholder="What's happening?" name="content"></textarea><br>
        <input type="file" name="image">
        <input type="submit" value="submit">
        <input type="hidden" name="token" value="<?= h($_SESSION['token']); ?>">
    </form>
    <p class="err"><?php echo h($app->getErrors('content')); ?></p>
    <script type="text/javascript">
    </script>



</body>
</html>
