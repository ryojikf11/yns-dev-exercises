<?php

namespace MyApp\Controller;

class Search extends \MyApp\Controller {

    public function run() {
        if (!$this->isLoggedIn()) {
          // login
          header('Location:' . APP. '/login.php');
          exit;
        }
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            if(isset($_POST["username"])){
                $this->searchUsername();

                // $this->setUsername();
            }
            if(isset($_POST["post"])){
                $this->searchPost();
            }
            if(isset($_POST['follow']) || isset($_POST['unfollow'])){
                $this->dis_followUnfollow();
                // header('Location:' . APP. '/follow.php');
                // return;
            }
        }

    }
    protected function dis_followUnfollow() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
        }
        if(isset($_POST['follow'])){
            // $this->check_count1();
            $this->follow_Process();
        }
        if(isset($_POST['unfollow'])){
            // $this->check_count2();
            $this->unfollow_process();
        }
    }
    protected function unfollow_process() {
        try {
            $deleteModel = new \MyApp\Model\Follow();
            $deleteModel->delete([
                'user_id' => $_SESSION['me'] ->user_id,
                'follow_id' =>$_POST['unfollow']
            ]);
        } catch (\MyApp\Exception\DatabaseError $e) {
            $this->setErrors('follow', $e->getMessage());
            return;
        }
    }
    protected function follow_process() {
        try {
            $insertModel = new \MyApp\Model\Follow();
            $insertModel->insertFollow([
                'user_id' => $_SESSION['me'] ->user_id,
                'follow_id' =>$_POST['follow']
            ]);
        } catch (\MyApp\Exception\DatabaseError $e) {
            $this->setErrors('follow', $e->getMessage());
            return;
        }
    }

    protected function searchPost() {
        try {
            $this->_validatePost();
        }catch (\MyApp\Exception\InvalidInput $e){
            $this->setErrors('post',$e->getMessage());
        }
        if($this->hasError()) {
            return;
        }else {
            try {
              $postsModel = new \MyApp\Model\Posts();
              $searchedPost = $postsModel->searchPost_DB([
                'post' => $_POST['post']
              ]);
              $this ->setValues('post', $searchedPost);
          } catch (\MyApp\Exception\NoResult $e) {
              $this->setErrors('post', $e->getMessage());
              return;
          }
        }

    }
    protected function searchUsername() {
        try {
            $this->_validateUsername();
        }catch (\MyApp\Exception\InvalidInput $e){
            $this->setErrors('username',$e->getMessage());
        }
        if($this->hasError()) {
            return;
        }else {
            try {
              $userModel = new \MyApp\Model\User();
              $usersname = $userModel->searchUsername_DB([
                'username' => $_POST['username']
              ]);
              $this ->setValues('usersname', $usersname);
              $this->setValues('follow',$userModel->getMyData());
          } catch (\MyApp\Exception\NoResult $e) {
              $this->setErrors('username', $e->getMessage());
              return;
          }
        }


    }
    private function _validateUsername() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if($_POST['username'] === null){
            throw new \MyApp\Exception\InvalidInput();
         }
         if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['username'])){
            throw new \MyApp\Exception\InvalidInput();
         }
    }
    private function _validatePost() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if($_POST['post'] === null){
            throw new \MyApp\Exception\InvalidInput();
         }
         if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['post'])){
            throw new \MyApp\Exception\InvalidInput();
         }
    }
}
