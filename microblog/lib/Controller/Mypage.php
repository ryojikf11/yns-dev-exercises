<?php
namespace MyApp\Controller;

class Mypage extends \MyApp\Controller {

  public function run() {
    if (!$this->isLoggedIn()) {
      // login
      header('Location:' . APP. '/login.php');
      exit;
    }
    // get my folloing
    $userModel = new \MyApp\Model\User();
    $this->setValues('follow',$userModel->getMyData());
    // get my post
    $postModel = new \MyApp\Model\Posts();
    $this->setValues('post',$postModel->getMyPost());
    //get my favorite
    $favoriteModel = new \MyApp\Model\Likes();
    $this->setValues('favorite',$favoriteModel->getMyFavorite());
  }
}
