<?php
namespace MyApp\Controller;

class Edit extends \MyApp\Controller {

    public function run()
    {
          // get users info
        if (!$this->isLoggedIn()) {
            // login
            header('Location:' . APP. '/login.php');
            exit;
        }
        //get Original
        $originalModel = new \MyApp\Model\Posts();
        $this->setValues('original',$originalModel->getOriginal());

        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            if(isset($_POST['return'])){
                unset($_SESSION['post_id']);
                header('Location:' . APP. '/mainpage.php');
            }else{
                $this->modifyProcess();
                // unset($_SESSION['edit_id']);
                // header('Location:' . APP. '/mainpage.php');
            }
        }
    }

    protected function modifyProcess()
    {
        try {
            $this -> _validate();
        } catch (\MyApp\Exception\InvalidContent $e){
            $this->setErrors('modify',$e->getMessage());
        }
        if($this->hasError()){
            return;
        }else{
        try {
                $modifyModel = new \MyApp\Model\Posts();
                $modifyModel->modifyPost([
                    'edit' => $_POST['edit']
                ]);
            } catch (\MyApp\Exception\DatabaseError $e) {
                $this->setErrors('modify', $e->getMessage());
            }
        }
        unset($_SESSION['post_id']);
        header('Location:' . APP. '/mainpage.php');
    }



    private function _validate()
    {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if($_POST['edit'] === "") {
            throw new \MyApp\Exception\InvalidContent();
         }
         if(!preg_match('/\A[a-zA-Z0-9 ]+\z/',$_POST['edit'])){
            throw new \MyApp\Exception\InvalidContent();
         }
    }


}
