<?php
namespace MyApp\Controller;

class Mainpage extends \MyApp\Controller {

    public function run() {
        if (!$this->isLoggedIn()) {
          // login
          header('Location:' . APP . '/login.php');
        }

        // know following
        $follow = $this->knowFollow();
        //get post from following and my post
        $posts = $this->getFollowPost($follow);

        //get comment related to the posts i got
        $commentModel = new \MyApp\Model\Comments();
        $this->setValues('comments',$commentModel->getComment());

        //retweet distinguish
        $retweetModel = new \MyApp\Model\Retweets();
        $this->setValues('myretweets',$retweetModel->checkMyRetweet());
        // check Likes
        $likeModel = new \MyApp\Model\Likes();
        $this->setValues('mylikes',$likeModel->checkMyLikes());
        //count Likes
        $this->setValues('countLikes',$likeModel->countLikes());

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if (isset($_POST['favorite'])) {
                $this->processLike();
                header('Location:' . APP. '/mainpage.php');
            } elseif (isset($_POST['unfavorite'])) {
                $this->processUnLike();
            } elseif (isset($_POST['comment'])) {
                $this->preCommentProcess();
                header('Location:' . APP. '/comment.php');
                return;
            } elseif (isset($_POST['edit'])) {
                $this->preEditProcess();
                header('Location:' . APP. '/edit.php');
                return;
            } elseif (isset($_POST['delete'])) {
                 $this->processDelete();
                 header('Location:' . APP. '/mainpage.php');
                 return;
            } elseif(isset($_POST['retweet'])) {
                $this->processRetweet();
                header('Location:' . APP. '/mainpage.php');
                return;
            } else {
                $this->processUnRetweet();
                header('Location:' . APP. '/mainpage.php');
                return;
            }
        }
    }
    protected function preCommentProcess()
    {
        $this->_validate();
        $_SESSION['post_id'] = $_POST['comment'];
    }

    protected function preEditProcess()
    {
        $this->_validate();
        $_SESSION['edit_id'] = $_POST['edit'];
    }

    protected function processDelete()
    {
        $this -> _validate();
        try {
          $deleteModel = new \MyApp\Model\Posts();
          $deleteModel->deletePost([
            'delete' => $_POST['delete']
        ]);
      } catch (\MyApp\Exception\DatabaseError $e) {
          $this->setErrors('favorite', $e->getMessage());
          return;
      }
    }
    protected function knowFollow()
    {
        try {
            $followModel = new \MyApp\Model\Follow();
            $following = $followModel->getfollow_db();
        } catch (\MyApp\Exception\DatabaseError $e) {
            $this->setErrors('content', $e->getMessage());
            return;
        }
        $array_follow = array();
        foreach($following as $follow){
            array_push($array_follow,$follow->follow_id);
        }
        $array_follow[] = $_SESSION['me']->user_id;
        $string = implode(",", $array_follow);
        return $string;
    }

    protected function getFollowPost($follow)
    {
        try {
            $postModel = new \MyApp\Model\Posts();
            $following = $postModel->getAllPost([
                'followAndMe' => $follow
            ]);
            $this->setValues('posts',$following);
        } catch (\MyApp\Exception\DatabaseError $e) {
            $this->setErrors('content', $e->getMessage());
            return;
        }
    }

    protected function processLike()
    {
        $this -> _validate();
        try {
              $likeModel = new \MyApp\Model\Likes();
              $likeModel->Like([
                'favorite' => $_POST['favorite']
              ]);
        } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('favorite', $e->getMessage());
              return;
        }
        header('Location:' . APP. '/mainpage.php');
        return;

    }

    protected function processUnLike()
    {
        $this -> _validate();
        try {
              $likeModel = new \MyApp\Model\Likes();
              $likeModel->UnLike([
                'unfavorite' => $_POST['unfavorite']
              ]);
        } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('favorite', $e->getMessage());
              return;
        }
        header('Location:' . APP. '/mainpage.php');
        return;

    }

    protected function processRetweet()
    {
            $this -> _validate();
            try {
              $retweetModel = new \MyApp\Model\Retweets();
              $retweetModel->Retweet([
                'retweet' => $_POST['retweet']
              ]);
          } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('favorite', $e->getMessage());
              return;
          }
    }
    protected function processUnRetweet()
    {
            $this -> _validate();
            try {
              $retweetModel = new \MyApp\Model\Retweets();
              $retweetModel->unRetweet([
                'unretweet' => $_POST['unretweet']
              ]);
          } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('favorite', $e->getMessage());
              return;
          }

    }
    private function _validate()
    {
        if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']) {
            echo "Invalid Token!";
            exit;
        }
    }


}
