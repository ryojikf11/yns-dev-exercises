<?php
namespace MyApp\Controller;

class Profile extends \MyApp\Controller {

  public function run()
    {
        if (!$this->isLoggedIn()) {
            // login
            header('Location:' . APP. '/login.php');
            exit;
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_FILES['image']['size'] !== 0) {
                //process including image
                $this->modifyProcessPic();
            } elseif (isset($_POST['return'])){
                header('Location:' . APP. '/mypage.php');
                return;
            } else {
                //process not including image
                 $this->modifyProcess();
            };
        }
    }

  protected function modifyProcess()
    {
        //validate
        try {
            $this -> _validate();
        } catch (\MyApp\Exception\InvalidUsername $e) {
            $this->setErrors('username',$e->getMessage());
        } catch (\MyApp\Exception\InvalidEmail $e) {
            $this->setErrors('email',$e->getMessage());
        }
        $this ->setValues('username', $_POST['username']);
        $this ->setValues('email', $_POST['email']);

            if ($this->hasError()) {
                return;
            } else {
              //create user
            try {
                $userModel = new \MyApp\Model\User();
                $userModel->modify([
                    'username' => $_POST['username'],
                    'email' => $_POST['email']
                ]);
            } catch (\MyApp\Exception\DuplicateEmail $e) {
                $this->setErrors('email', $e->getMessage());
                return;
            }
            $_SESSION['successPro'] = 'Your Profile change is success';
            header('Location:' . APP. '/mypage.php');
            return;
        }
    }
  protected function modifyProcessPic()
    {
        try {
            $this -> _validate();
        } catch (\MyApp\Exception\InvalidUsername $e){
            $this->setErrors('username',$e->getMessage());
        } catch (\MyApp\Exception\InvalidEmail $e){
             $this->setErrors('email',$e->getMessage());
        }
        try{
            // error check
            $this->_validateUpload();
            // type check
            $ext = $this->_validateImageType();
            // save
            $savePath =$this->_save($ext);
        } catch (\MyApp\Exception\InvalidPicture $e) {
            $this->setErrors('email',$e->getMessage());
        }
        if ($this->hasError()) {
            return;
        }
        $this->savePath =$savePath;
        if ($this->hasError()) {
            return;
        } else {
            //create user
            try {
                $userModel = new \MyApp\Model\User();
                $userModel->modifyPic([
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'profile_path'=> $savePath
                ]);
            } catch (\MyApp\Exception\DuplicateEmail $e) {
                $this->setErrors('email', $e->getMessage());
                return;
            }
            $_SESSION['successPro'] = 'Your Profile change is success';
            header('Location:' . APP. '/mypage.php');
            return;
        }
    }
    private function _validateUpload()
    {
        if (!isset($_FILES['image']) || !isset($_FILES['image']['error'])) {
            throw new \MyApp\Exception\InvalidPicture();
        }
        if (explode("/",$_FILES['image']['type'])[0] != "image") {
            throw new \MyApp\Exception\InvalidPicture();
        }
        switch($_FILES['image']['error']){
            case UPLOAD_ERR_OK:
            return true;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
            throw new \MyApp\Exception\InvalidPicture();
            default:
            throw new \MyApp\Exception\InvalidPicture();
        }
    }

    private function _validateImageType(){
        $this->_imageType = exif_imagetype($_FILES['image']['tmp_name']);
        switch($this->_imageType){
            case IMAGETYPE_GIF:
            return 'gif';
            break;
            case IMAGETYPE_JPEG:
            return 'jpeg';
            break;
            case IMAGETYPE_PNG:
            return 'png';
            break;
            default:
            throw new \MyApp\Exception\InvalidPicture();
        }
    }

    private function _save($ext) {
        $this->_imageFileName = sprintf(
              '%s_%s.%s',
              time(),
              sha1(uniqid(mt_rand(), true)),
            $ext
      );
      $uploaddir = '/var/www/html/yns-dev-exercises/microblog_ryoji/microblog/profile/';
      $savePath = $uploaddir  . $this->_imageFileName;
      $res = move_uploaded_file($_FILES['image']['tmp_name'], $savePath);
      if($res === false) {
          throw new \MyApp\Exception\InvalidPicture();
      }
      return $savePath;
  }
  private function _validate() {
      if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
          echo "Invalid Token!";
          exit;
      }
      if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['username'])){
          throw new \MyApp\Exception\InvalidUsername();
      }

      if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
          throw new \MyApp\Exception\InvalidEmail();
      }

  }

}


 ?>
