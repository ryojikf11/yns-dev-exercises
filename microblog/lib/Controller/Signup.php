<?php

namespace MyApp\Controller;

class Signup extends \MyApp\Controller {
    public function run() {
        if ($this->isLoggedIn()) {
          // login
          header('Location:' . APP. '/post.php');
          exit;
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            $this->postProcess();
        }
}

    protected function postProcess() {
        $this -> _validate();
        if($this->hasError()) {
            return;
        } else {
            $this ->setValues('username', $_POST['username']);
            $this ->setValues('email', $_POST['email']);
            //create user
            try {
                $userModel = new \MyApp\Model\User();
                $userModel->create([
                    'username' => $_POST['username'],
                    'email' => $_POST['email'],
                    'password' => $_POST['password']
                ]);
            } catch (\MyApp\Exception\DuplicateEmail $e) {
                $this->setErrors('email', $e->getMessage());
                return;
            }

            //redirect to login
            $_SESSION['successSig'] = 'Your Signup is success';
            header('Location:' . APP. '/login.php');
            return;
        }

    }

    private function _validate() {
        if(!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
        }
        if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['username'])){
            $this->setErrors('username','invalid username');
        }
        if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL)) {
            $this->setErrors('email','invalid email');
        }
        if(!preg_match('/\A[a-zA-Z0-9]+\z/',$_POST['password'])){
            $this->setErrors('password','invalid password');
        }
        if($_POST['password'] !== $_POST['confirm_password']){
            $this->setErrors('password','password doesnot match');
        }
    }
}


 ?>
