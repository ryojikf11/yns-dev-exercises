<?php
namespace MyApp\Controller;

class Comment extends \MyApp\Controller {

    public function run()
    {
        // get users info
        if (!$this->isLoggedIn()) {
            // login
            header('Location:' . APP. '/login.php');
            exit;
        }
        if($_SERVER['REQUEST_METHOD'] === 'POST'){
            if (!isset($_POST['comment']) ) {
                echo 'null';
            } elseif(isset($_POST['return'])) {
                unset($_SESSION['post_id']);
                header('Location:' . APP. '/mainpage.php');
                return;
            } else {
                $this->commentProcess();
                $_SESSION['successCom'] = 'Your Comment is success';
            }
        }
    }

    protected function commentProcess()
    {
        try {
            $this -> _validate();
        } catch (\MyApp\Exception\InvalidContent $e) {
            $this->setErrors('comment',$e->getMessage());
        }
        if ($this->hasError()) {
            return;
        } else {
        try {
                $commentModel = new \MyApp\Model\Comments();
                $commentModel->insertComment([
                    'comment' => $_POST['comment']
                ]);
            } catch (\MyApp\Exception\DatabaseError $e) {
                $this->setErrors('comment', $e->getMessage());
                return;
            }
                header('Location:' . APP. '/mainpage.php');
        }
    }

    private function _validate()
    {
        if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']){
            echo "Invalid Token!";
            exit;
         }
         if (!preg_match('/\A[a-zA-Z0-9 ]+\z/',$_POST['comment'])) {
            throw new \MyApp\Exception\InvalidContent();
         }
    }
}
