<?php
namespace MyApp\Controller;

class Post extends \MyApp\Controller {

  public function run()
    {
          // get users info
        if (!$this->isLoggedIn()) {
          // login
          header('Location:' . APP. '/login.php');
          exit;
        }
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {
            if ($_POST['content'] && $_FILES['image']['size'] !== 0) {
                $this->set_post();
                $path = $this->set_pic();
                $this->createBoth($path);

            };
            if ($_POST['content'] && $_FILES['image']['size'] === 0) {
                 $this->set_post();
                 $this->createContent();
            };
            if (!$_POST['content'] && $_FILES['image']['size'] !== 0) {
                 $path = $this->set_pic();
                 $this->createPicture($path);
            }
            if (!$_POST['content'] && $_FILES['image']['size'] === 0) {
                 $this->setErrors('content','empty post');
            }
        }
    }
    protected function set_post()
    {
        //validate
        try {
            $this -> _validate();
        } catch (\MyApp\Exception\InvalidContent $e) {
            $this->setErrors('content',$e->getMessage());
        }
    }
    protected function set_pic() {
        try {
            // error check
            $this->_validateUpload();
            // type check
            $ext = $this->_validateImageType();
            // save
            $savePath =$this->_save($ext);
          } catch (\MyApp\Exception\InvalidPicture $e) {
              $this->setErrors('content',$e->getMessage());
              return;
          }
            return $savePath;
    }

    private function _validateUpload() {
        if (!isset($_FILES['image']) || !isset($_FILES['image']['error'])) {
            throw new \MyApp\Exception\InvalidPicture();
        }
        if (explode("/",$_FILES['image']['type'])[0] != "image") {
            throw new \MyApp\Exception\InvalidPicture();
        }
        switch ($_FILES['image']['error']) {
            case UPLOAD_ERR_OK:
            return true;
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
            throw new \MyApp\Exception\InvalidPicture();
            default:
            throw new \MyApp\Exception\InvalidPicture();
        }
    }

    private function _validateImageType()
    {
        $this->_imageType = exif_imagetype($_FILES['image']['tmp_name']);
        switch($this->_imageType){
            case IMAGETYPE_GIF:
            return 'gif';
            break;
            case IMAGETYPE_JPEG:
            return 'jpeg';
            break;
            case IMAGETYPE_PNG:
            return 'png';
            break;
            default:
            throw new \MyApp\Exception\InvalidPicture();
        }
    }

    private function _save($ext)
    {
        $this->_imageFileName = sprintf(
            '%s_%s.%s',
            time(),
            sha1(uniqid(mt_rand(), true)),
            $ext
        );
        $uploaddir = '/var/www/html/yns-dev-exercises/microblog_ryoji/microblog/images/';
        $savePath = $uploaddir . $this->_imageFileName;
        $res = move_uploaded_file($_FILES['image']['tmp_name'], $savePath);
        if ($res === false) {
            throw new \MyApp\Exception\InvalidPicture();
        }
        return $savePath;
    }

    private function _validate()
    {
        if (!isset($_POST['token']) || $_POST['token'] !== $_SESSION['token']) {
            echo "Invalid Token!";
            exit;
        }
        if (!preg_match('/\A[a-zA-Z0-9 ]+\z/',$_POST['content'])) {
            throw new \MyApp\Exception\InvalidContent();
        }
        $limit = 140;
        if (mb_strlen($_POST['content']) > $limit) {
            throw new \MyApp\Exception\InvalidContent();
        }
    }

    protected function createBoth($path)
    {
        if ($this->hasError()) {
            return;
        } else {
            //create post
            try {
                $postModel = new \MyApp\Model\Posts();
                $postModel->postBoth([
                    'content' => $_POST['content'],
                    'picturePath' => $path,
                    'user_id' => $_SESSION['me']->user_id
                ]);
            } catch (\MyApp\Exception\DatabaseError $e) {
                $this->setErrors('content', $e->getMessage());
                return;
            }
            $_SESSION['successPos'] = 'Your Post is success';
            header('Location:' . APP. '/mainpage.php');
            return;
        }
    }

    protected function createPicture($path){
        if ($this->hasError()) {
            return;
        } else {
            //create post
            try {
              $postModel = new \MyApp\Model\Posts();
              $postModel->postPicture([
                'picturePath' => $path,
                'user_id' => $_SESSION['me']->user_id
              ]);
          } catch (\MyApp\Exception\DatabaseError $e) {
              $this->setErrors('content', $e->getMessage());
              return;
            }

            $_SESSION['successPos'] = 'Your Post is success';
            header('Location:' . APP. '/mainpage.php');
        }
    }

    protected function createContent(){
        if ($this->hasError()) {
            return;
        } else {
            //create post
            try {
                $setContent = $this->content;
                $userModel = new \MyApp\Model\Posts();
                $userModel->postContent([
                    'content' => $_POST['content'],
                     'user_id' => $_SESSION['me']->user_id
                ]);
            } catch (\MyApp\Exception\DatabaseError $e) {
                    $this->setErrors('content', $e->getMessage());
                    return;
            }

            $_SESSION['successPos'] = 'Your Post is success';
            header('Location:' . APP. '/mainpage.php');
            return;
        }
    }

}
