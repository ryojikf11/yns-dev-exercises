<?php

    namespace MyApp\Model;

    class User extends \MyApp\Model {
        public function create($values){
            $stmt = $this->db->prepare("insert into user_info (username,email, password,created,modified) values (:username,:email, :password,now(),now())");
            $res = $stmt -> execute([
                ':username' => $values['username'],
                ':email' => $values['email'],
                ':password' => password_hash($values['password'], PASSWORD_DEFAULT)
            ]);
            if($res === false){
                throw new \MyApp\Exception\DuplicateEmail();
            }
        }

        public function login($values) {
            $stmt = $this->db->prepare("select * from user_info where email = :email");
            $stmt->execute([
                ':email' => $values['email']
            ]);
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            $user = $stmt->fetch();
            if(empty($user)) {
                throw new \MyApp\Exception\UnmatchEmailOrPassword();
            }
            if(!password_verify($values['password'],$user->password)) {
                throw new \MyApp\Exception\UnmatchEmailOrPassword();
            }
            return $user;
        }
        public function findAll() {
            $stmt = $this->db->query("select * from user_info");
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
        public function searchUsername_DB($values) {
            $stmt = $this->db->prepare("select * from user_info where username like :username");
            $bindvalue = "%" .$values['username'] . "%";
            $stmt->execute([
                ':username' => $bindvalue
            ]);
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
        public function getMyData() {
            $me = $_SESSION['me']->user_id;
            $stmt = $this->db->prepare("select * from follow  inner join user_info  on follow.follow_id = user_info.user_id  where follow.user_id = $me");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
        public function modify($values) {
            $myid = $_SESSION['me']->user_id;
            $myname = '"' .$values['username']. '"';
            $myemail = '"' .$values['email']. '"';
            $stmt = $this->db->prepare("update user_info set username = $myname,email = $myemail where user_id = $myid");
            $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function modifyPic($values) {
            $myid = $_SESSION['me']->user_id;
            $myname = '"' .$values['username']. '"';
            $myemail = '"' .$values['email']. '"';
            $myprofile = '"' .$values['profile_path']. '"';
            $stmt = $this->db->prepare("update user_info set username = $myname,email = $myemail,profile_image = $myprofile where user_id = $myid");
            $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
    }

?>
