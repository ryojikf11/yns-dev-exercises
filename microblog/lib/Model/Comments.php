<?php

    namespace MyApp\Model;

    class Comments extends \MyApp\Model {
        public function insertComment($values){
        $me = $_SESSION['me']->user_id;
        $comment = '"' .$values['comment']. '"';
        $post = $_SESSION['post_id'];
        $stmt = $this->db->prepare("insert into comments (comment,post_id,user_id,created,modified) values ($comment,$post,$me,now(),now())");
        $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function getComment(){
            $stmt = $this->db->query("select * from comments inner join user_info on comments.user_id = user_info.user_id  order by comments.created desc");
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
    }
?>
