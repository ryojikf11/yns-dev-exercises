<?php

    namespace MyApp\Model;

    class Posts extends \MyApp\Model {
        public function postContent($values){
            $stmt = $this->db->prepare("insert into posts (content,created,modified,user_id) values (:content,now(),now(),:user_id)");
            $res = $stmt -> execute([
                ':content' => $values['content'],
                ':user_id' => $values['user_id']
            ]);
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function postPicture($values){
            $stmt = $this->db->prepare("insert into posts (image_path,created,modified,user_id) values (:picturePath ,now(),now(),:user_id)");
            $res = $stmt -> execute([
                ':picturePath' => $values['picturePath'],
                ':user_id' => $values['user_id']
            ]);
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function postBoth($values){
            $stmt = $this->db->prepare("insert into posts (content,image_path,created,modified,user_id) values (:content,:picturePath ,now(),now(),:user_id)");
            $res = $stmt -> execute([
                ':content' => $values['content'],
                ':picturePath' => $values['picturePath'],
                ':user_id' => $values['user_id']
            ]);
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function searchPost_DB($values) {
            $bindvalue =  '"%' .$values['post']. '%"';
            $stmt = $this->db->prepare("select * from posts inner join user_info  on posts.user_id = user_info.user_id where content like $bindvalue order by posts.created desc");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
        public function getAllPost($values){
            $bindvalue = "(" .$values['followAndMe']. ")";
            echo $bindvalue;
            $me = $_SESSION['me']->user_id;
            $stmt = $this->db->prepare("select * FROM  (SELECT posts.important,posts.content,posts.image_path,posts.user_id,user_info.username,user_info.profile_image,posts.modified,posts.retweet,posts.retweetby,posts.status
                                        FROM posts INNER JOIN user_info on posts.user_id = user_info.user_id
                                        where  posts.user_id IN $bindvalue and posts.status = 1
                                        UNION all
                                        SELECT posts.important,posts.content, posts.image_path,posts.user_id,user_info.username,user_info.profile_image,retweets.created,retweets.retweet,retweets.retweeter_name,posts.status
                                        from posts  inner join retweets  on retweets.post_id = posts.important
                                        INNER JOIN user_info ON posts.user_id = user_info.user_id
                                        where retweets.user_id IN  $bindvalue)
                                        a order BY modified desc");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
        public function getOriginal(){
            $post = $_SESSION['edit_id'];
            $stmt = $this->db->prepare("select content from posts where posts.important = $post");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetch();
        }
        public function getMyPost(){
            $me = $_SESSION['me']->user_id;
            $stmt = $this->db->prepare("select * from posts where posts.user_id = $me order by posts.created desc");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
        }
        public function modifyPost($values){;
            $post = '"' .$values['edit'] .'"';
            $post_id = $_SESSION['edit_id'];

            $stmt = $this->db->prepare("update posts set content = $post,modified = now() where important = $post_id");
            $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function deletePost($values){
            $post_id = $values['delete'];
            $stmt = $this->db->prepare("update posts set status = 0 where important = $post_id");
            $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
    }

?>
