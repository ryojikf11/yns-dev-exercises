<?php

    namespace MyApp\Model;

    class Likes extends \MyApp\Model {
        public function Like($values){
            $me = $_SESSION['me']->user_id;
            $favorite = $values['favorite'];
            $true = 1;
            $stmt = $this->db->prepare("insert into likes (user_id,post_id,likes) values ($me ,$favorite,$true)");
            $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function checkMyLikes(){
            $me = $_SESSION['me']->user_id;
            $stmt = $this->db->prepare("select * from likes where likes.user_id = $me");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function getMyFavorite(){
            $me = $_SESSION['me']->user_id;
            $stmt = $this->db->prepare("select * from likes inner join posts on likes.post_id = posts.important where likes.user_id = $me");
            $stmt->execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
                if($res === false){
                    throw new \MyApp\Exception\DatabaseError();
                }
        }
        public function UnLike($values){
            $me = $_SESSION['me']->user_id;
            $post = $values['unfavorite'];
            $stmt = $this->db->prepare("delete from likes where user_id = $me and post_id = $post");
            $res = $stmt -> execute();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }
        public function countLikes(){
            $stmt = $this->db->prepare("select post_id, COUNT(post_id) as 'check'  from likes GROUP BY post_id;");
            $res = $stmt -> execute();
            $stmt->setFetchMode(\PDO::FETCH_CLASS, 'stdClass');
            return $stmt->fetchAll();
            if($res === false){
                throw new \MyApp\Exception\DatabaseError();
            }
        }


    }

?>
