SELECT CONCAT(first_name,last_name) AS "fullname", hire_date
FROM employees
ORDER BY hire_date ASC
LIMIT 1
